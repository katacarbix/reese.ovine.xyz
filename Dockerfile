FROM httpd:2-alpine
ADD dist /usr/local/apache2/htdocs
ADD httpd.conf /usr/local/apache2/conf/
