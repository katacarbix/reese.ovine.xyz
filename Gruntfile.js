const sass = require('sass');

module.exports = function(grunt){
	grunt.initConfig({
		sass: {
			options: {
				implementation: sass,
				outputStyle: 'compressed'
			},
			dist: {
				files: {
					'dist/css/style.css': 'src/sass/style.scss'
				}
			}
		},
		watch: {
			sass: {
				files: 'src/sass/**/*',
				tasks: ['sass']
			}
		},
		browserSync: {
			dev: {
				bsFiles: {
					src: [
						'dist/**/*'
					]
				},
				options: {
					watchTask: true,
					server: {
						baseDir: 'dist'
					}
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['browserSync', 'watch']);
}
